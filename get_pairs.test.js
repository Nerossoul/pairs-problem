const getPairs = require("./get_pairs");

test("is good pairs generated", () => {
  const mocks = [
    {
      sum: 5,
      input: [22, 3, 5, 0, 2, 2],
      output: [
        [3, 2],
        [5, 0]
      ]
    },
    {
      sum: 5,
      input: [-5, 33, 2, 2, 3, 5, 0, 10, 3],
      output: [
        [-5, 10],
        [2, 3],
        [2, 3],
        [5, 0]
      ]
    },
    {
      sum: 5,
      input: [5, 5, 5, 0, 0, 0, 5],
      output: [
        [5, 0],
        [5, 0],
        [5, 0]
      ]
    },
    {
      sum: 6,
      input: [3, 3, 6, 0],
      output: [
        [3, 3],
        [6, 0]
      ]
    }
  ];
  for (let i = 0; i < mocks.length; i += 1) {
    expect(getPairs(mocks[i].sum, mocks[i].input)).toEqual(mocks[i].output);
  }
});
