function getPairs(sum, numbers) {
  const checkPair = (compareNumber, pair, nextElement) => {
    let newPair = [...pair];
    let isUsed = false;
    switch (pair.length) {
      case 0:
        newPair = [nextElement];
        isUsed = true;
        break;
      case 1:
        if (nextElement + pair[0] === compareNumber) {
          newPair.push(nextElement);
          isUsed = true;
        }
        break;
    }
    return { newPair: newPair, isUsed: isUsed };
  };

  const reducer = (accumulator, currentValue) => {
    let isUsed = false;
    let checkResult = accumulator.map(pair => {
      if (!isUsed) {
        let checkResult = checkPair(sum, pair, currentValue);
        isUsed = checkResult.isUsed;
        return checkResult.newPair;
      }
      return pair;
    });
    if (!isUsed) {
      checkResult.push([currentValue]);
    }
    return checkResult;
  };

  let pairs = numbers.reduce(reducer, []);

  pairs = pairs.filter(pairElement => pairElement.length === 2);

  return pairs;
}

module.exports = getPairs;
