You have an array of ints. Create function which groups numbers by sum of pairs
(sum = 5). Each element of array has to be used only once.
---
test data:
input: [22, 3, 5, 0, 2, 2]
resutls: [[3, 2], [5, 0]]
input: [-5, 33, 2, 2, 3, 5, 0, 10, 3]
resutls: [[-5, 10], [2, 3], [2, 3], [5, 0]]
input: [5, 5, 5, 0, 0, 0, 5]
resutls: [[5, 0], [5, 0], [5, 0]]
sum=6
input: [3, 3, 6, 0]
resutls: [[3, 3], [6, 0]]


Language: JavaScript

Deadline: 1 day - DONE

----
### Get started

` npm install `

### Testing

` npm run test `